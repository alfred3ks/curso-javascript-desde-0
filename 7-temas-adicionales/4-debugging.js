/*
Debugging: Vamos a ver como hacer debugging usando JavaScript:
*/

const usuario = {
  nombre: '',
  edad: 0,
  acceso_permitido: true,
};

// Registramos los datos del usuario
document.getElementById('deb').addEventListener('click', () => {
  usuario.nombre = prompt('Ingresa tu nombre');
  /*
  📌 Mediante consola
  */
  console.log(usuario);
  usuario.edad = prompt('Ingresa tu edad');
  console.log(usuario);

  /*
  📌 Mediante palabra debugger, cuando el código llega ahi se para:
  */
  // debugger;

  /*
  📌 Mediante breakpoint
  Debemos ir a la consola de desarrolladores / Fuentes/ seleccionamos el archivo.
  */

  if (usuario.edad >= 18) {
    usuario.acceso_permitido = true;
  } else {
    usuario.acceso_permitido = false;
  }
});
