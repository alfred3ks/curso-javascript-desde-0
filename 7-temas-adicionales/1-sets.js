/*
Sets:
Es una especie de arreglo especial donde solo podemos guardar únicos.
*/

// Crear un set: Se eliminan los valores duplicados.
const colores = new Set(['rojo', 'verde', 'azul', 'rojo']);
console.log(colores); //  {'rojo', 'verde', 'azul'}

// Métodos para agregar valores al set: Al final
colores.add('amarillo');
console.log(colores);

// Podemos iterar un set:
colores.forEach((color) => {
  console.log(color);
});

// En un set podemos guardar cualquier tipo de dato:
const numeros = new Set([1, 2, 3, 2, 3, 4]);
console.log(numeros);
