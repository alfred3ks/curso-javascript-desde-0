/*
📌 this:
Palabra reservada this.
La palabra reservada this representa algo distinto dependiendo de donde se utiliza.
Vamos a ver todos los casos posibles.
*/

/*
📌 Fuera de cualquier bloque, representa el objeto global (window), esto en el navegador.
*/

console.log(this); // Objeto global window

/*
📌 Dentro de un funcion de tipo flecha, representa el objeto global.
*/

const palabraThis = () => {
  console.log(this);
};
palabraThis();

/*
📌 En un evento this se refiere al elemento que recibio el evento.
Nota: Solo cuando la función no es de tipo flecha.
Si es de tipo flecha se referira al objeto global.
*/

const btn = document.getElementById('btn1');
btn.addEventListener('click', function () {
  // El boton es this
  console.log(this);
});

btn.addEventListener('click', () => {
  // El objeto global
  console.log(this);
});

/*
📌 Cuando lo usamos dentro de un objeto en un metodo this es el objeto
*/

class Alumno {
  constructor(nombre, curso) {
    this.nombre = nombre;
    this.curso = curso;
    console.log(this);
  }

  // Método:
  saludo() {
    console.log(this);
    console.log(`Hola soy ${this.nombre} y estoy en el curso de ${this.curso}`);
  }
}

const mario = new Alumno('Carlos', 'JavaScript');
mario.saludo();
