/*
maps: Lo mapas en javascript.
Los mapas son como objetos pero donde las propiedades pueden ser de cualquier tipo.
*/

// Un Objeto:
const persona = {
  nombre: 'Luis',
};

console.log(persona);

// Un Mapa:
const mapa = new Map([
  ['nombre', 'Luis'],
  [true, 'verdadero'],
  [persona, { pass: 123 }],
]);

console.log(mapa);

// Metodos para obtener sus valores del MAPA:
console.log(mapa.get('nombre'));
console.log(mapa.get(true));
console.log(mapa.get(persona));

// Los MAPAS tambien los podemos iterar con forEach():
mapa.forEach((valor, propiedad) => {
  console.log(`Propiedad: ${propiedad} Valor: ${valor}`);
});
