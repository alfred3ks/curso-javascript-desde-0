/*
Vamos a ver la APi de fetch():

Esta tambien es una APi del navegador que podemos usar para hacer peticiones a servidores y estos nos devuelven información.
Vamos a ver como leer informción desde el siguiente servidor:

https://www.npoint.io/

Una página que nos permitira subir nuestro json y podemos leerlo desde ahi:
Vamos pegamos nuestro archivo json, damos a Create JSON Bin, luego vamos a Share y obtenemos la url:

https://api.npoint.io/4fd9e214b378914e437b


*/

const endPoint = 'https://api.npoint.io/4fd9e214b378914e437b';

const peticion = fetch(endPoint);
console.log(peticion); // Responde una promesa:

peticion
  .then((respuesta) => {
    // Retorna una promesa: usando el metodo .json()
    const promesa = respuesta.json();
    return promesa;
  })
  .then((datos) => {
    console.log('Obteniendo datos con then()...');
    console.log(datos);
  })
  .catch((err) => {
    console.log(err);
  });

// Vemos con Async/Await:
const peticionAsync = async () => {
  try {
    console.log('Obteniendo datos Async/Await...');
    const peticion = await fetch(endPoint);
    const respuesta = await peticion.json();
    console.log(respuesta);
  } catch (error) {
    console.log(error);
  }
};

peticionAsync();
