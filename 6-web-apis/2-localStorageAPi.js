/*
La APi de localStorage:
Vamos a ver como trabajar con la API del navegador localStorage.
Con esta APi podemos guardar de forma local información y si el usuario refresca la página, o si la cierra, esta información no se borra.
*/

/*
Vamos a guardar el nombre que introduce el usuario en localStorage
*/

// Asi guardamos informacion en localStorage:
document.getElementById('btn1').addEventListener('click', (e) => {
  const nombre = prompt('Introduce tu nombre:');
  console.log(nombre);

  /*
  Asi guardamos en localStorage un valor, Asi accedemos a esa API:
  Para ver donde se guarda esa información, en el navegador, en la ventana de desarrollo: Aplicacion/Almacenamiento/Almacenamiento local
  La cantidad que guarda localStore es de 5MB.
  */
  localStorage.setItem('usuario', nombre);
  location.reload();
  console.log(localStorage.usuario);
});

// Asi accedemos a ese valor desde código:
if (localStorage.usuario) {
  console.log(
    'Hola soy el usuario ' +
      localStorage.usuario +
      ' y estoy guardado en localStore'
  );
} else {
  console.log('Hola, no hay usuario guardado en localStorage');
}

// Asi eliminamos esa información del localStorage:
document.getElementById('btn2').addEventListener('click', (e) => {
  // Pasamos como string la llave:
  localStorage.removeItem('usuario');
  location.reload();
});
