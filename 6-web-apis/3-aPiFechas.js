/*
APi de fechas:

Dentro del navegador web tambien tenemos un APi para trabajar con fechas. Vamos a ver como podemos usar esta APi.

Tenemos la documentacion en la MDN:

https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/Date

Esta APi es bastante compleja y podemos hacer muchas cosas. Para profundizar ahi esta la documentación.

*/

// Lo primero que debemos hacer es crear un objeto {} de fecha:
const date = new Date();
console.log(date); // retorna un objeto con la fecha actual, incluye toda la información sobre la fecha.

// Podemos traer una variable y la transformamos a el objeto fecha: asi tendremos acceso a todos los métodos del objeto fecha:
const cumpleanos = new Date('1975-08-14T15:25:00');
console.log(cumpleanos);
console.log(typeof cumpleanos);

// Métodos para fechas:
// Transformar el objeto fecha a string:
console.log(date.toString());
console.log(date.toDateString());
console.log(date.toTimeString());
console.log(date.getMonth() + 1); // OJO las fechas es como los arreglos empliezan en 0, retorna es la posiccion:
console.log(date.getDate());
console.log(date.getFullYear());

/*
Tambien es muy normal usar librerias externas para trabajar el tema de fechas. Por ejemplo tenemos la libreria date-fn:

https://date-fns.org/

*/
