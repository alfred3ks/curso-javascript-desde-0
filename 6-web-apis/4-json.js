/*
JSON:
json = JavaScript Object Notation,

Esto es practicamente un objeto de JavaScript. Donde pondremos guardar información.
Tiene una estructura algo diferente a los objetos de JavaScript, pero bastante similar.

Este objeto es el que se utiliza para mandar información de un sitio a otro.Es el usado para acceder a información de las APis externas.

Todos los lenguajes de programación tienen funciones para poder leer este objeto.
Este objeto ha venido a sustituir a XML.

En un objeto JSON solo podemos poner ciertos tipos de dato:
- cadenas de texto,
- números,
- objetos (en formato JSON),
- arreglos,
- booleanos,
- null.

Este los objetos de JSON no podemos usar:
- funciones,
- objetos de fecha,
- undefined

Podemos ver el archivo data.json

Reglas para poder crear un archivo json:
- Pares clave:valor, JSON está conformado por datos en clave/valor:

  name: "eugenia",
  password: "eugenia123",
  country: "spain",
  age: 27

Representación de datos fácil de leer y entender.

- Las claves deben ir en doble comillas tambien: Si o si debe ser así:

  "name": "eugenia",
  "password": "eugenia123",
  "country": "spain",
  "age": 27

- Cualquier clave que creemos siempre, siempre debe tener un valor. Podemos crear
una clave con un valor vacío:

  "nickname": ""

- Colocar todos estos clave/valor entre llaves: así agrupamos todos los datos en una sola entidad:

{
  "name": "eugenia",
  "password": "eugenia123",
  "country": "spain",
  "age": 27
}

- Para separar los claves valor unos de otros debemos usar coma, "," salvo el último
par clave valor que no llevara.

- Podemos anidar objetos, uno dentro de otros:

{
  "name": "eugenia",
  "password": "eugenia123",
  "address": {
  "city": "london",
  "street": "baker street 123"
},
  "age": 27
}

- Las extensiones de los archivos deben ser .json:

datos.json
mydata.json
users.json

- Cuando enviamos datos de un sistema a otro por lo general le damos información al otro sistema que datos le estamos enviando, eso se conoce como mime type, o el
tipo de datos de archivos que estoy enviando, en json es común usar:

application/json

*/

// Vamos a leer el archivo data.json:
// Peticions realizada a un servidor y nos responde esto:
const datos = `{
  "id": 1,
  "nombre": "Luis",
  "suscriptor_activo": true,
  "post": [
    { "id": 1, "titulo": "Primer post", "texto": "Texto del primer post..." },
    { "id": 2, "titulo": "Segundo post", "texto": "Texto del segundo post..." }
  ]
}`;

// Transformamos este objeto JSON a Objeto de JS: Dos métodos muy usados:
/*
📌 JSON.parse():
Le pasamos una cadena de texto con formato de JSON y nos retorna un objeto de JavaScript.
*/
const dataObj = JSON.parse(datos);
console.log(dataObj);
console.log(dataObj.nombre);

/*
📌 JSON.stringify():
Nos permite pasarle un objeto de JavaScript y nos devuelve una cadena de texto con un objeto en formato JSON.
*/

const dataJSON = JSON.stringify(dataObj);
console.log(dataJSON);
