/*
Validación de formularios:

Existen varias formas de validar la información que se capta de un formularios. Aqui en este veremos una forma, pero en el siguiente lo veremos con expresiones regulares.

Vamos a validar la información al hacer click del boton de enviar:
Este es un ejemplo de una validación sencilla. Lo ideal es usar expresiones regulares.
*/

const formulario = document.getElementById('formulario-donacion');
// console.log(formulario);

formulario.addEventListener('submit', (e) => {
  e.preventDefault();

  // Empezamos a captar los datos del formulario:
  const dataForm = {
    correo: formulario.correo.value,
    pais: formulario.pais.value,
    donacion: formulario.donacion.value,
    fecha: formulario.fecha.value,
    terminos: formulario['terminos-y-condiciones'].checked,
  };

  // validamos los datos:
  if (dataForm.correo.length <= 2) {
    console.log('Correo invalido');
    return;
  }

  if (dataForm.pais === '') {
    console.log('Pais invalido, selecciona un país');
    return;
  }

  if (dataForm.donacion === '') {
    console.log('Donación invalido selecciona un valor a donar');
    return;
  }

  if (dataForm.fecha === '') {
    console.log('Fecha invalida, selecciona una fecha');
    return;
  }

  if (!dataForm.terminos) {
    console.log('Terminos invalidos, acepta los términos y condiciones');
    return;
  }

  console.log('Datos enviados...', dataForm);
});
