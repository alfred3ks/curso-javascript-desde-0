/*
Eventos de formularios:
Vamos a ver los diferentes eventos para formulario que tenemos:

Existen muchos eventos para formularios, pero veremos los mas usados.

*/

// Obtenemos el formulario: Por medio del objeto forms:
const formulario = document.forms['formulario-donacion'];
// console.log(formulario);

/*
📌 Evento submit:
Nos permite detectar cuando el se hace click al enviar el formulario:
*/

formulario.addEventListener('submit', (e) => {
  // console.log('Click');
  // Prevenimos el envio del formulario con el método preventDefault():
  e.preventDefault();

  // Ya podemos acceder a los valores del formulario:

  const donacion = formulario.donacion;
  console.log(donacion.value);
});

/*
📌 Evento change:
Nos permite detectar cuando hay un cambio en el valor de un input, select o textarea.
Se ejecuta este evento cuando cambiamos el valor del input y le damos click fuera.
*/

const pais = formulario.pais;
// console.log(pais);
pais.addEventListener('change', (e) => {
  console.log('El pais cambio.');
  // Vemos dos formas de acceder al valor:
  console.log(`El pais seleccionado es ${pais.value}`);
  console.log(`El pais seleccionado es ${e.target.value}`);
});

// Veamos como seria con los radio button: accedemos asi porque es una palabra de varias letras el id:
const radioButton5 = formulario['cantidad-5'];
const radioButton10 = formulario['cantidad-10'];

radioButton5.addEventListener('change', (e) => {
  console.log('La cantidad cambio 5');
  console.log(`El valor seleccionado es ${e.target.value}`);
  console.log(`El valor seleccionado es ${radioButton5.value}`);
});

radioButton10.addEventListener('change', (e) => {
  console.log('La cantidad cambio a 10');
  console.log(`El valor seleccionado es ${e.target.value}`);
  console.log(`El valor seleccionado es ${radioButton10.value}`);
});

/*
📌 Evento focus:
Nos permite detectar cuando un input recibe el foco de atencion:
*/

const correo = formulario.correo;
// console.log(correo);

correo.addEventListener('focus', (e) => {
  console.log('Hacemos focus sobre el input del correo');
  console.log(`El valor del input del correo es ${correo.value}`);
  console.log(`El valor del input del correo es ${e.target.value}`);
});

/*
📌 Evento blur:
Nos permite detectar cuando un input pierde el foco de atencion:
*/

correo.addEventListener('blur', (e) => {
  console.log('El input correo pierde el foco "blur"');
});

/*
📌 Evento keydown:
Nos permite detectar cuando se presiona una tecla sobre el input:
*/

correo.addEventListener('keydown', (e) => {
  console.log(e);
  console.log(`accedemos a la tecla presionada: ${correo.value}`);
});

/*
📌 Evento keyup: mmm este me gusta.
Se ejecuta cuando una tecla es levantada al escribir sobre el input:
*/

correo.addEventListener('keyup', (e) => {
  console.log(e);
  console.log(`accedemos a la tecla: ${correo.value}`);
});
