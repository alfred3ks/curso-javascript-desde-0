/*
Validación del formulario mas avanzada usadno expresiones regulares.

Las expresiones regulares son pequeñas formulas que podemos usar para buscar si un texto coincide con esa formula.

En el siguiente recurso online podemos validar crear nuestras expresiones regulares, tambien se la podemos preguntar a CHATGPT.

https://regexr.com/
https://chat.openai.com/

*/

const formulario = document.getElementById('formulario-donacion');
// console.log(formulario);

formulario.addEventListener('submit', (e) => {
  e.preventDefault();

  // Empezamos a captar los datos del formulario:
  const dataForm = {
    correo: formulario.correo.value,
    pais: formulario.pais.value,
    donacion: formulario.donacion.value,
    fecha: formulario.fecha.value,
    terminos: formulario['terminos-y-condiciones'].checked,
  };

  // validamos los datos: Con Regex

  const expresionRegularCorreo =
    /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$/;
  /*
  Con el metodo test() evaluamos la expresion, retorna true si es corecto false si no es correcto.
  */
  if (!expresionRegularCorreo.test(dataForm.correo)) {
    console.log('Correo inválido, selecciona una correcto');
    return;
  }

  if (dataForm.pais === '') {
    console.log('Pais inválido, selecciona un país');
    return;
  }

  if (dataForm.donacion === '') {
    console.log('Donación inválida, selecciona una cantidad a donar');
    return;
  }

  if (dataForm.fecha === '') {
    console.log('Fecha inválida, selecciona una fecha');
    return;
  }

  if (!dataForm.terminos) {
    console.log('Terminos inválidos, acepta los términos y condiciones');
    return;
  }

  console.log('Datos enviados...', dataForm);
});
