/*
Accediendo a los elementos del formularios:
Tenemos dos formas de acceder a formularios:
- Mediante el objeto forms, este es un objeto que JS construye y contiene todos los formularios de la página.
- Mediante métodos del DOM, ya hemos visto como acceder a elementos del DOM.
Ambas muy usadas.
*/

/*
📌 Mediante el objeto forms: forms:
Nos retorna una coleccion de HTML de elementos del formulario, recuerda si hay varios formulario retornara todos los de la página.

NOTA: Cuando en un formulario tenemos un boton que esta dentro de las etiquetas form, este va a tratar de enviar el formulario, lo vemos cuando se recarga la página, lo enviara a donde le especifiquemos en action, el atributo de elemento form.
*/

// Accedemos al objeto forms, una coleccion HTML de todos los formularios.
const forms = document.forms;
console.log(forms);
// Accedemos al formulario:
console.log(forms['formulario-donacion']);
console.log(forms['formulario-donacion']['correo']);
// accedemos al valor que se mete en el input:
console.log(forms['formulario-donacion']['correo'].value);

/*
📌 Mediante métodos del DOM:
*/

const formulario = document.querySelector('#formulario-donacion');
console.log(formulario);

const correo = document.querySelector('#formulario-donacion #correo');
console.log(correo);
console.log(correo.value);

/*
📌 Veamos como acceder a todos los valores a partir del boton de enviar:
Lo haremos agregando un addEventListert()
NOTA: Cuando en un formulario tenemos un boton que esta dentro de las etiquetas form, este va a tratar de enviar el formulario, lo vemos cuando se recarga la página, lo enviara a donde le especifiquemos en action, el atributo de elemento form.
*/
const botonEnviar = document.getElementById('btnEnviar');
console.log(botonEnviar);

botonEnviar.addEventListener('click', (e) => {
  // Evitamos que se refresque la página:
  e.preventDefault();

  // ahora ya podemos obtener los valores del formulario:
  const correo = document.querySelector('#formulario-donacion #correo');
  console.log(correo.value);

  // Obtenemos los valores del select:
  const select = document.querySelector('#formulario-donacion #pais');
  console.log(select.value);

  // Obtenemos los valores de los radio buton:
  const inputRadio5 = document.querySelector(
    '#formulario-donacion #cantidad-5'
  );
  const inputRadio10 = document.querySelector(
    '#formulario-donacion #cantidad-10'
  );

  inputRadio5.checked
    ? console.log(inputRadio5.value)
    : console.log(inputRadio10.value);

  // Obtenemos el valor de la fecha:
  const fecha = document.querySelector('#formulario-donacion #fecha');
  console.log(fecha.value);

  // Obtenemos los terminos y condiciones:
  const terminos = document.querySelector(
    '#formulario-donacion #terminos-y-condiciones'
  );
  terminos.checked
    ? console.log('Acepta los términos.')
    : console.log('No acepta los terminos.');
});
