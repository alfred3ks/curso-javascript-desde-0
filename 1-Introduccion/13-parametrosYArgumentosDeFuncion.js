/*
Parámetros y argumentos de función.

- Los parametros son los que pasamos en los parentesis de la funcion.
- Los argumentos son los que pasamos cuando invocamos la función.
*/

// Aqui vemos como pasamos los parametros, y parametros por defecto.
const saludo = (nombre = 'Jonh Doe') => {
  console.log(`Hola ${nombre}`);
};

// Aquí vemos como pasamos argumentos:
saludo('Pepe');
saludo('Juan');
saludo('Mario');
saludo();

// Aqui vemos que podemos pasar multiples parametros/argumentos:
const operacion = (num1, num2, tipo) => {
  if (tipo === 'suma') {
    console.log(num1 + num2);
  } else if (tipo === 'resta') {
    console.log(num1 - num2);
  } else {
    console.log('Datos errones...');
  }
};

operacion(4, 7, 'suma');
operacion(4, 7, 'resta');
operacion(4, 7);
