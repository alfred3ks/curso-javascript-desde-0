/*
Break y Continue.
Son dos instrucciones muy usadas dentro de los ciclos.
*/

/*
📌 Break.
Detiene la ejecución de un ciclo e funcion de una condición.
La sentencia break nos sirve para salir de bloques de tipo switch. Pero tambien sirve para forzar la salida de un ciclo.
*/

const nombres = [
  'Arturo',
  'Andres',
  'Alejandro',
  'Robert',
  'Adrian',
  'Antonio',
  'Angel',
];

for (let i = 0; i < nombres.length; i++) {
  if (nombres[i][0] !== 'A') {
    console.log('ALTO Hay un nombre que no empieza con A');
    console.log(`El nombre es: ${nombres[i]} `);
    break;
  }
  console.log(`El nombre es: ${nombres[i]}`);
}

/*
📌 Continue.
La sentencia continue nos sirve para saltar a la siguiente iteración.
*/
const invitados = [
  'Carlos',
  'Cristina',
  'Christofer',
  'Jorge',
  'Estefania',
  'Erika',
  'Manuel',
];

console.log('Lista de personas aceptadas.');
for (let i = 0; i < invitados.length; i++) {
  if (invitados[i] === 'Jorge') {
    continue;
  }
  console.log(`Invitados: ${invitados[i]}`);
}
