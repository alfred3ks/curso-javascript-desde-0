/*
Los condicionales.

Estos nos permiten ejecutar codigo en funcion de una condición.

Operadores de comparación:
Nos permiten comparar valores.

== -> Igual que,
=== -> Igual en valor y tipo.
!= -> Diferente,
!== -> Diferente en valor y tipo.
> -> Mayor,
>= Mayor o igual que,
< -> Menor,
<= -> Menor o igual que,
? -> Operador ternario

Operadores lógicos:

&& -> AND,
|| -> OR,
! -> NOT
*/

// Estructura de un condiciona:

// if(condicion){
//   code....
// }

const usuario = {
  edad: 19,
  pais: 'italia',
  ticket: true,
};

// Ejemplo #1:
if (usuario.edad > 17) {
  console.log('Puedes entrar al concierto.');
} else {
  console.log('No puedes entrar al concierto, eres menor de edad.');
}

// Ejemplo #2:
if (usuario.edad > 17 && usuario.ticket) {
  console.log(
    'Puedes entrar al concierto, eres mayor de edad y tienes un ticket.'
  );
} else if (usuario.edad > 17 || usuario.ticket) {
  console.log('No puedes entrar, eres mayor de edad pero no tienes ticket.');
} else if (usuario.edad < 17 && usuario.ticket) {
  console.log('No puedes entrar, eres menor de edad pero si tienes ticket.');
} else {
  console.log('No puedes entrar, eres menor y no tienes ticket.');
}

// Ejemplo ·#3: anidando condicionales:
if (usuario.edad >= 18) {
  if (usuario.ticket) {
    console.log('El usuario es mayor de edad y tiene un ticket.');
  } else {
    console.log('El usuario es mayor de edad pero no tiene ticket.');
  }
} else {
  console.log('El usuario es menor de edad.');
}

// Ejemplo #4:
if (usuario.pais === 'portugal') {
  console.log('El usuario es de Portugal.');
} else if (usuario.pais === 'colombia') {
  console.log('El usuario es de Colombia.');
} else if (usuario.pais === 'españa') {
  console.log('El usuario es de España.');
} else {
  console.log('El usuario no tiene pais.');
}
