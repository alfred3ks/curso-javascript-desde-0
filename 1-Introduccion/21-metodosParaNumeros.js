/*
Métodos para trabajar con números.
*/

/*
📌 .toString(): Método:
Convierte de tipo numeros a cadenas de texto.
*/
const numero = 18;
console.log(numero, typeof numero);
const numberText = numero.toString();
console.log(numberText, typeof numberText);

/*
📌 .toFixed(): Método:
Permite obtener un numero con la cantidad de decimales especificados.
*/
const PI = 3.141617;
let numberFixed = PI.toFixed(0);
console.log(numberFixed);
numberFixed = PI.toFixed(2);
console.log(numberFixed);
numberFixed = PI.toFixed(10);
console.log(numberFixed);

/*
📌 .parseInt(): Es una funcion, no un método:
Intenta transformar un valor a un entero. Normalmente los datos que vienen de un formulario viene siendo texto.
*/
const numberA = '12';
const numberParseInt = parseInt(numberA);
console.log(numberParseInt);

/*
📌 .parseInt(): Es una funcion, no un método:
Intenta transformar un valor a un entero con decimales. Normalmente los datos que vienen de un formulario viene siendo texto.
*/
const numberB = '123.567';
const numberParseFloat = parseFloat(numberB);
console.log(numberParseFloat);

/*
📌 .Math.random(): Método: Math es un objeto que tenemos en JS que tiene metodos.
Genera un numero al azar del 1 al 0.
*/
const numeroAzar = Math.random();
console.log(numeroAzar);

/*
📌 .Math.floor(): Método: Math es un objeto que tenemos en JS que tiene metodos.
Redondea un numero hacia abajo.
*/
const numberC = 12.367;
const numberFloor = Math.floor(numberC);
console.log(numberFloor);

/*
📌 .Math.ceil(): Método: Math es un objeto que tenemos en JS que tiene metodos.
Redondea un numero hacia arriba.
*/
const numberCeil = Math.ceil(numberC);
console.log(numberCeil);

/*
📌 .Math.round(): Método: Math es un objeto que tenemos en JS que tiene metodos.
Redondea hacia el entero mas cercano.
*/
const numberRound = Math.round(numberC);
console.log(numberRound);

/*
📌 Ejemplo como obtener un numero al azar del 1 al 100 y del 0 al 100
*/
const numDel1Al100 = Math.round(Math.random() * 100);
console.log(`El numero del uno al cien al azar es:${numDel1Al100}`);

const numDel0Al100 = Math.floor(Math.random() * 101);
console.log(`El numero del cero al cien al azar es:${numDel0Al100}`);
