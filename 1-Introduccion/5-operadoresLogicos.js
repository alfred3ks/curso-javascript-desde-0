/*
Operadores lógicos:

&& -> AND,
|| -> OR,
! -> NOT
*/

// Veamos con un ejemplo, queremos vendre boletos de entrada a un concierto y solo se venderan a mayores de edad.

const nombre = 'Carlos';
const edad = 17;
const tieneEntrada = true;
const tienePermiso = true;

// Con AND && se tiene que cumplir ambas condiciones.
permitirAcceso = edad >= 18 && tieneEntrada;
console.log('Acceso permitivo al concierto: ' + permitirAcceso);

// Con OR ||se tiene que cumplir alguna de las condiciones:
permitirAcceso = (edad >= 18 && tieneEntrada) || (tienePermiso && tieneEntrada);
console.log('Acceso permitivo al concierto: ' + permitirAcceso);

// Con el NOT negamos la condicion:
let variable = true;
console.log(!variable);
variable = false;
console.log(!variable);
