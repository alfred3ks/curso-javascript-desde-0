/*
Ciclo for.
Vamos a ver los ciclos dentro de Javascript.
Los ciclos son estructuras que nos van a permitir repetir bloques de código cuantas veces lo necesitemos. Tambien son muy usados para recorrer arreglos.
*/

const nombres = [
  'Carlos',
  'Christian',
  'Christofer',
  'Estefania',
  'Erika',
  'Manuel',
];

/*
📌 Ciclo for:
Repite un bloque de código miestras se cumpla una condición.
- Expresion 1: Se ejecuta una vez antes de comenzas a repetir el bloque de código.
- Expresion 2: Una condición, mientras se cumpla se ejecutará el bloque de código.
- Expresion 3: Esta expresión se ejecuta siempre y despues de que se ejecute el bloque de código.
*/
// Como mostrar numeros del 0 al 10
for (let numero = 0; numero <= 10; numero++) {
  console.log(numero);
}

// Como mostrar numeros del 0 al 100 de 5 en 5
for (let numero = 0; numero <= 100; numero += 5) {
  console.log(numero);
}

// Como mostrar numeros del 50 al 0
for (let numero = 50; numero >= 0; numero--) {
  console.log(numero);
}

// Vemos como recorremos el arreglo
for (let nombre = 0; nombre < nombres.length; nombre++) {
  console.log(`El nombre: ${nombres[nombre]}`);
}
