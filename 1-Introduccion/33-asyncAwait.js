/*
Async await.
Es una sintaxis más moderna para hacer peticiones asincronas. Necesitamos try-catch para ejecutar la promesa y poder gestionar los errores.
*/

const fetchPosts = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const posts = ['Post 1', 'Post 2', 'Post 3'];
      const error = false;

      if (error) {
        reject('Hubo un error al intentar de obtener los posts.');
      } else {
        resolve(posts);
      }
    }, 2000);
  });
};

// Así consumimos la promesa con async-await y try-catch para gestionar la respuesta.
const mostrarPosts = async () => {
  try {
    const posts = await fetchPosts();
    console.log(posts);
  } catch (error) {
    console.log(error);
  }
};

mostrarPosts();
