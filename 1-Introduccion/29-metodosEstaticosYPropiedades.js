/*
Métodos estáticos y propiedades.
*/

class User {
  // Propiedad estatica:
  static registrados = 1000;

  constructor(name, email) {
    this.name = name;
    this.email = email;
  }

  // Metodo estatico:
  static deleteUser(id_user) {
    console.log(
      `El usuario con id: ${id_user}, ha sio borrado de la base de datos.`
    );
  }
}

const user1 = new User('Kaila', 'kaila@correo.com');
console.log(user1);

// Asi accedemos al metodo estatico: No tenemos que crear un nuevo objeto, llamanos la clase y luego el metodo o propiedad, esto es lo mismo que pasa con la clase Math().
User.deleteUser(12);
console.log(User.registrados);
