/*
Métodos para strings.
*/

/*
📌 .length: propiedad:
Nos devuelve el numero de caracteres de una cadena de texto.
*/
const text = 'Hola, yo soy un dev de JavaScript';
console.log(text.length);

/*
📌 .indexOf() .lastIndexOf(): Métodos:
Nos devuelve el index del primer/último caracter especificado. Nos retorn el indice de la letra que buscamos, si retorna -1 es que no esta presente
*/
const textMin = text.toLowerCase();
console.log(textMin);
const firstIndex = textMin.indexOf('l');
console.log(firstIndex);

const lastIndex = textMin.lastIndexOf('o');
console.log(lastIndex);

/*
📌 .slice(): Método:
Devuelve un fragmento de una cadena de texto.
- 1er parametro - index desde donde queremos cortar.
- 2do parametros (opcional) - index hasta donde queremos cortar. siempre va al index - 1
*/
const indexY = text.indexOf('y');
console.log(indexY); // 6
const indexV = text.indexOf('v');
console.log(indexV); // 18
const newStringIndex = text.slice(indexY, indexV + 1);
const newString = text.slice(6, 19);
console.log(newStringIndex);
console.log(newString);

/*
📌 .replace(): Método:
Devuelve una cadena de text en donde reemplazamos un valor por otro..
- 1er parametro - el texto que queremos reemplazar.
- 2do parametros  - el texto que queremos poner
*/
const textDev = 'Hola, yo soy un developer de Python';
console.log(textDev);
const newTextDev = textDev.replace('Python', 'JavaScript');
console.log(newTextDev);

/*
📌 .split(): Método:
Convierte una cadena de texto en un arreglo. Tenemos que especificar donde cortar cada elemento.
1er parametro - el caracter que funcionara como separador. Normalmente es espacio que los separa.
*/
const textArr = textDev.split(' ');
console.log(textArr);

/*
📌 .toUpperCase() .toLowerCase(): Método:
Devuelve una cadena de texto en minuscula/mayuscula.
*/
const textDevMin = textDev.toLowerCase();
console.log(textDevMin);
const textDevMay = textDev.toUpperCase();
console.log(textDevMay);
