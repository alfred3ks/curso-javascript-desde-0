/*
El operador ternario lo podemos entender como una estructura compacta para hacer condicionales. Consiste en una expresión que se evaluará y, dependiendo de si dicha evaluación fue positiva o negativa devolverá una u otra cosa.
*/

let boleto = 'VIP';
boleto = 'REGULAR';
let codigoDeAcceso;
const vip = 'VIP-123-456';
const regular = 'REGULAR-456-789';

codigoDeAcceso = boleto === 'VIP' ? `${vip}` : `${regular}`;

console.log(codigoDeAcceso);
