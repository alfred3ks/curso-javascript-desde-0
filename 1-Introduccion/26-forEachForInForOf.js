/*
Ciclo forEach(), for of y for in.
*/

/*
📌 Ciclo forEach():
Es un metodo de los arreglos que podemos usar para recorrer los elementos.
*/

const amigos = ['Alejandro', 'Cesar', 'Manuel'];

amigos.forEach((amigo, index) => {
  console.log(`El amigo número ${index} es ${amigo}`);
});

/*
📌 Ciclo for in:
Nos permite recorrer las propiedades de un objeto.
*/
const persona = {
  nombre: 'Lucia',
  edad: 27,
  correo: 'lucia@correo.com',
};

for (const propiedad in persona) {
  console.log(propiedad);
}

/*
📌 Ciclo for of:
Nos permite recorrer los valores de un objeto iterable.
Podemos recorrer arreglos, cadenas de texto, mapas y lista de nodos, etc.
*/
for (const amigo of amigos) {
  console.log(amigo);
}

// Vemos como recorrer una collection HTML:
let etiquetas = document.head.children;
console.log(etiquetas);

for (const etiqueta of etiquetas) {
  console.log(etiqueta);
}

// Otra opción transforma el HTMLCollection a arreglo y luego si ejecutarlos con los metodos de arreglos.
etiquetas = [...etiquetas];
console.log(etiquetas);

etiquetas.forEach((etiqueta) => {
  console.log(etiqueta);
});
