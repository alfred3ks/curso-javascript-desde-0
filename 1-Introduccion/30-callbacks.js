/*
Callbacks.
Son funciones que podemos pasar como parametro a otra función. Una función ejecuta otra función.
*/

/*
📌 Ejemplo:
*/
const obtenerPostDeUsuarios = (usuario, callback) => {
  console.log(`Obteniendo los post de ${usuario}...`);
  setTimeout(() => {
    let posts = ['Post1', 'Post2', 'Post3'];
    callback(posts);
  }, 3000);
};

obtenerPostDeUsuarios('carlos', (posts) => {
  console.log(posts);
});
