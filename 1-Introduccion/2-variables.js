/*
Las variables en JavaScript.
Las variables son espacios en memoria en memoria donde podemos guardar información que vamos a utilizar para realizar diversar operaciones.

Asi definimos las variables: Le asignamos un valor. Podemos usar las palabras reservadas var, let o const para declarar y/o asignar variables.

Las variables deben seguir unas reglas al ser declaradas:

- Deben empezar por letras, no pueden empezar por números, por simbolos, solo pueden empezar por dos simbolos, el $ -> dolar, y el _ -> guion bajo.

- Las variables deben tener nombre unico. No podemos crear variables que se llamen igual.

- Las variables no deben usarse las palabras reservas del lenguaje como var, for, const, etc.

Dentro de las variables podemos guardas distintos tipos de datos:
- string -> cadena de texto
- number -> números
- boolean -> booleanos(true or false)
- object -> objetos {}
- function -> funciones

- null -> Valor nulo
- undefined -> valor sin definir, es que asigna JS a las variables que estan sin definir.

Usaremos let y const para declarar variables. La sintaxis mas moderna, let para las variables que pueden cambiar y const para las constantes que no deben cambiar.
*/

// Usando var
var edad = 22;
console.log(edad);

// Usando let y const
let nombre = "Bruce";
const correo = "correo@correo.com";
console.log(nombre, correo);

/*
Podemos declarar asi en una misma linea separadas por comas.
*/

let telefono, pais, id;
telefono = 123123123;
pais = "España";
id = 123;

console.log(telefono, pais, id);

/*
Con las variables podemos hacer tambien operaciones aritmeticas, sumar, restar, multiplicar, dividir.
*/

// Suma
// Numeros:
const resultado = 4 + 4;
console.log(resultado);

// Cadenas:
const nombre1 = "Luis";
const nombre2 = "Arturo";

const nombreCompleto = nombre1 + " " + nombre2;
console.log(nombreCompleto);

/*
En js podemos cambiar el valor de una variable asignandole un nuevo valor: JS lo permite, otros lenguajes no `permiten hacer esto.
*/

let miVariable = "texto";
console.log(miVariable);
miVariable = 123; // cambiamos el tipo y valor.
console.log(miVariable);
