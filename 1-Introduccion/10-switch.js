/*
Veremos otro tipo de condicional. El switch. Es muy similar al if...else.

Para este tipo de condicional es muy importante la sentencia break.
*/

const usuario = {
  nombre: 'Layka',
  edad: 2,
  pais: 'España',
};

switch (usuario.pais) {
  case 'España':
    console.log(`El usuario ${usuario.nombre}, es de ${usuario.pais}`);
    break;
  case 'Italia':
    console.log(`El usuario ${usuario.nombre}, es de ${usuario.pais}`);
    break;
  case 'Portugal':
    console.log(`El usuario ${usuario.nombre}, es de ${usuario.pais}`);
    break;
  case 'Francia':
    console.log(`El usuario ${usuario.nombre}, es de ${usuario.pais}`);
    break;
  default:
    console.log(
      `El usuario ${usuario.nombre}, es de otro pais: ${usuario.pais}`
    );
}
