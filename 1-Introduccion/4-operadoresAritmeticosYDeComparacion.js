/*
Operadores aritméticos y de comparación.

Estos nos ayudaran a realizar calculos básicos y diversas operaciones.
Estos se dividen en diferentes categorías.

- Operadores Aritmeticos:
= -> Asignación, usado para asignar valores a una variable,
+ -> Suma,
- -> Resta,
* -> Multiplicación,
/ -> División,
% -> Módulo,
++ -> Incremento,
-- -> Decremento.
*/

// Asignación:
const age = 22;

// Suma:
const suma = 10 + 10;
console.log(suma);

// Resta:
const resta = 23 - 7;
console.log(resta);

// Multiplicación:
const multiplicacion = 23 * resta;
console.log(multiplicacion);

// División:
const division = multiplicacion / suma;
console.log(division);

// Módulo:
const modulo = 10 % 3;
console.log(modulo);

// Incremento:
let numero = 12;
console.log(numero);
numero = ++numero; // numero = numero + 1
console.log(numero);

// Decremento:
let price = 99.9;
console.log(price);
price = --price; // price = price - 1
console.log(price);

/*
Operadores de Asignación:
+= Suma un numero al valor de una variable,
-= Resta un numero al valor de una variable,
*= Multiplica un numero al valor de una variable,
/= Divide un numero al valor de una variable,
%= Obtiene el sobrante de una division y lo asignamos a la variable.
*/

let number = 10;

number += 5;
console.log(number);

number -= 3;
console.log(number);

number *= 3;
console.log(number);

number /= 4;
console.log(number);

number %= 3;
console.log(number);

/*
Operadores de comparación:
Nos permiten comparar valores.

== -> Igual que,
=== -> Igual en valor y tipo.
!= -> Diferente,
!== -> Diferente en valor y tipo.
> -> Mayor,
>= Mayor o igual que,
< -> Menor,
<= -> Menor o igual que,
? -> Operador ternario
*/

let result = 5 > 1;
console.log(result);

result = 20 > 20;
console.log(result);

result = 20 >= 20;
console.log(result);

result = 20 < 10;
console.log(result);

result = 20 <= 12;
console.log(result);

result = 10 == '10';
console.log(result);

result = 10 === '10';
console.log(result);

result = 10 != '10';
console.log(result);

result = 10 !== '10';
console.log(result);

// Operador ternario: Condicionales de una sola linea.
result =
  7 > 1
    ? 'El primer valor es mayor que el segundo'
    : 'El segundo valor es mayor que el primero';
console.log(result);

result = 7 > 1 ? true : false;
console.log(result);
