/*
Las clases dentro de JavaScript.
Las clases son plantillas que vamos a poder utilizar para crear objetos. En estas clases podemos encapsular código.
*/

/*
📌 Estructura de una clase:
- Definición. Palabra reservada class con el nombre de esa clase, normalmente se pone con la primer letra en mayuscula.
-Propiedades: La clase puede contener variables. Dentro de una clase se llaman propeidades.
- Constructor: Es un método especial para inicalizar un objeto creado a partir de una clase.
- Métodos: La clase puede contener funciones. Dentro de una clase se llaman métodos.
*/

class Usuario {
  // Propiedades:
  tipo = 'usuario';

  // Método especial constructor:
  constructor(nombre, apellido) {
    this.nombre = nombre;
    this.apellido = apellido;
  }

  // Aqui vemos los métodos:
  obtenerNombreCompleto() {
    console.log('Obteniendo datos...');
    return `${this.nombre} ${this.apellido}`;
  }
}

// Asi creamos objetos a partir de la plantilla.
const pedro = new Usuario('Pedro', 'Rodríguez');
console.log(pedro);
console.log(typeof pedro);
console.log(pedro.obtenerNombreCompleto());

const alejandro = new Usuario('Alejandro', 'Perez');
console.log(alejandro);
console.log(typeof alejandro);
console.log(alejandro.obtenerNombreCompleto());
