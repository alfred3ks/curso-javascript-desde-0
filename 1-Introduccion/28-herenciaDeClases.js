/*
Herencia de clases.
Nos permite crear clases tomando las propiedades y métodos de una clase padre.
*/

class User {
  constructor(user, password) {
    this.user = user;
    this.password = password;
  }

  obtenerPost() {
    const post = ['post1', 'post2'];
    return post;
  }
}

// Aqui vemos la herencia, hereda propiedades y metodos de Usuario
class Moderator extends User {
  constructor(user, password, permissions) {
    // super copia los del padre por herencia, esta es la forma de hacerlo:
    super(user, password);
    this.permissions = permissions;
  }

  deletePost(id) {
    if (this.permissions.includes('delete')) {
      console.log(`El post con ${id} ha sido borrado.`);
    } else {
      console.log(`No tienes los permisos para borrar post.`);
    }
  }
}

// Usuario normal:
const user1 = new User('Angerl', '123');
console.log(user1.obtenerPost());

// Moderador:
const user2 = new Moderator('Luis', '234', ['delete', 'edit']);
console.log(user2.obtenerPost());
console.log(user2.permissions);
user2.deletePost(12);
