/*
La sentencia return en una funcion. Es una palabra reservada del lenguaje que podemos usar para develver información.
Siempre siempre la funcion debe retornar una sola cosa. Solo debemos tener un solo return. Recibe muchos parametros y retorna una sola cosa.

NOTA: Todo lo que pongamos por debajo de la sentencia return ese código no se ejecutara. Ahi se para la ejecución del código.
*/

const operacion = (num1, num2, tipo) => {
  let resultado;
  if (tipo === 'suma') {
    resultado = num1 + num2;
  } else if (tipo === 'resta') {
    resultado = num1 - num2;
  } else {
    resultado = 'Datos erroneos...';
  }
  return resultado;
};

let suma = operacion(4, 7, 'suma');
console.log(suma);
let resta = operacion(4, 7, 'resta');
console.log(resta);
let erroneo = operacion(4, 7);
console.log(erroneo);
