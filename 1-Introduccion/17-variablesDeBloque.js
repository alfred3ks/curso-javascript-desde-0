/*
Variables de bloque.
Este nuevo scope llego cuando se agregaron las declaraciones de variables usando cont y let.

📌 Block Scope / alcance de tipo bloque:
- Pertenecen las variables declaradas con const o let dentro de un bloque {}
- Solo podemos acceder a ellas dentro del bloque.
*/

// Ejemplo #1:
const edad = 22;

if (edad >= 18) {
  // Variable de bloque solo se puede acceder a ella dentro de las {} del if
  const accesoPermitido = true;

  if (true) {
    console.log(accesoPermitido);
  }

  const miFuncion = () => {
    console.log(accesoPermitido);
  };

  miFuncion();
}

// Variable global:
const accesoPermitido = 'SI';

// No puede acceder a la variable dentro de las {}, si es con var si:
console.log(accesoPermitido);

// Ejemplo #2: aqui vemos como con var si no hay alcance de bloque:
if (true) {
  var nombre = 'Mario';
  console.log(nombre);
}

console.log(nombre);
