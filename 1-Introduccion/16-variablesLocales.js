/*
Variables locales.
Ahora veremos el scope o alcance local.

📌 Local Scope o Variables Locales:
- Son variables declaradas dentro de una función.
- Solo podemos acceder a ellas desde dentro de la función.
*/

var obtenerNumeroLetras = (texto) => {
  // Esta variable tiene scope local, y solo se puede acceder a ella dentro de la función:
  var numero = texto.length;
  console.log(` El texto ${texto} tiene ${numero} letras`);

  var funcionAnidada = () => {
    // Aqui vemos como podemos acceder a esa variable:
    console.log(numero);
  };
  funcionAnidada();
};

obtenerNumeroLetras('Alfredo');
