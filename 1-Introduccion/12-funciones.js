/*
Funciones.
Las funciones son bloques de código que vamos a definir lineas de codigo y vamos a poder llamar cuando lo queramos. Tambien nos permite reutilizar código y hacer este mas dinámico.
*/

// Definimos una función: Función como declaración.
// Forma #1:
function saludo() {
  console.log('Hola, estoy dentro de una función.');
}

saludo();

// Forma #2: Asignando una función a una variable: función anonima. Función como expresion:
const saludo1 = function () {
  console.log('Hola desde una función asígnada a una variable.');
};

saludo1();

// Forma #3: Funciones tipo flecha:
const saludoFlecha = () => {
  console.log('Hola desde una función de tipo flecha.');
};

saludoFlecha();
