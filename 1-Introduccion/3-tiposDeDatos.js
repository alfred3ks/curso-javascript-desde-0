/*
Tipos de datos:

Dentro de las variables podemos guardas distintos tipos de datos:
- string -> cadena de texto
- number -> números
- boolean -> booleanos(true or false)
- object -> objetos {}
- function -> funciones

- null -> Valor nulo
- undefined -> valor sin definir, es que asigna JS a las variables que estan sin definir.
*/

// String, cadenas de texto: Podemos usar comillas simples o dobles. Da igual, pero usar solo una de ellas en el código. Si tenemos prettier le podemos decir que comillas usar.

const nombre = 'Andres';
const parrafo = 'Hola esto "es comillas dentros"';

// Asi podemos escapar comillas simples dentro de simples, OJO prettier lo cambia como arriba.

// const parrafoEscapado = 'Asi podemos escapar parrafos \' Asi podemos poner comillas simples dentro de simples\'';

// Number, de tipo numero:

const numero = 4;
const numero1 = 3.1456;

// Booleanos, true or false:

const isDev = true;
const isUserRegister = false;
const userConect = true;

const mayorQue = 1 > 2;
console.log(mayorQue); // false

// Arreglos o arrays: [] Variable que guarda multiples valores:

const data = [1, 10, 5];
console.log(data);

const multipleData = ['texto', 456, true, { nombre: 'Frank' }, [1, 2, 3]];

/*
Los objetos: {} Son muy similares a los arreglos, nos permiten guardar multiple informacion en parejas de propiedad:valor.
*/

const player = {
  name: 'Michael',
  lastName: 'Jordan',
  number: 23,
  age: 36,
  team: 'Chicago Bulls',
  active: false,
};

console.log(player);
console.log(player.name);

/*
function, las funciones, son bloques de codigo que podemos reutilizar.
*/

function saludar() {
  console.log('Hola, es un mensaje desde una función..');
}

saludar();

// null y undefined:

const variableNull = null;
console.log(variableNull);

let variableUndefined;
console.log(variableUndefined);
