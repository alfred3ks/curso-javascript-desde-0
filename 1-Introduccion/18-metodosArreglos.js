/*
Métodos para arreglos. []

Dentro de JavaScript los arreglos son objetos, y los objetos tienen propiedades y métodos.
*/

// Definimos nuestro array:
const colores = ['rojo', 'azul', 'verde'];
console.log(colores);

/*
📌 .length: propiedad
- Nos permite conocer la cantidad de elementos de un arreglo.
*/
console.log(colores.length);

/*
📌 .toString(): Método:
- Nos permite transformar un arreglo a una cadena de texto.
- Por ejemplo para poder mostrarlo en el navegador.
*/
const cadena = colores.toString();
document.write(cadena);

/*
📌 .join(): Método:
- Nos permite transformar un arreglo a una cadena de texto y separar cada elemento.
*/
const arrJoin = colores.join(', ');
console.log(arrJoin);

/*
📌 .sort(): Método:
- Nos permite ordenar un arreglo de cadenas de texto de forma alfabetica.
*/
const letras = ['z', 'w', 'f', 'c', 'd', 'a'];
const arrSort = letras.sort();
console.log(arrSort);

/*
📌 .reverse(): Método:
- Nos permite ordenar un arreglo de forma descendente.
*/
const arrReverse = letras.reverse();
console.log(arrReverse);

/*
📌 .concat(): Método:
- Nos permite juntar dos arreglos en uno solo.
*/
const arr1 = [1, 2, 3];
const arr2 = ['a', 'b', 'c'];
const arr3 = arr1.concat(arr2);
console.log(arr3);

/*
📌 .push(): Método:
- Nos permite agregar elemento al final del arreglo.
*/
colores.push('amarillo', 'negro', 'naranja', 'violeta');
console.log(colores);

/*
📌 .pop(): Método:
- Nos permite eliminar el ultimo elemento de un arreglo.
*/
colores.pop();
console.log(colores);

/*
📌 .shift(): Método:
- Elimina el primer elemento de un arreglo, y retorna ese elemento eliminado, ademas recorre el resto de elementos del array.
*/
const dias = [
  'Lunes',
  'Martes',
  'Miercoles',
  ' Jueves',
  'Viernes',
  'Sabado',
  'Domingo',
];

console.log(dias);
const diaEliminado = dias.shift();
console.log(diaEliminado);
console.log(dias);

/*
📌 .unshift(): Método:
- Agrega un elemento al inicio del arreglo y empuja al resto de elementos.
*/
dias.unshift('Lunes');
console.log(dias);

/*
📌 .splice(): Método:
Nos permite insertar elementos a un arreglo donde le especifiquemos. Recibe tres argumentos.
- 1er parametro - Posición donde queremos comenzar a insertar los elementos.
- 2do parametros - Si queremos eliminar elementos del arreglo desde la posición indicada.
- Resto de parametros - Los elementos a insertar.
*/
const amigos = ['Alejandro', 'Cesar', 'Manuel'];
console.log(amigos);

amigos.splice(1, 2, 'Rafael', 'Roberto');
console.log(amigos);
// Aqui no eliminamos solo agregamos
amigos.splice(1, 0, 'Maria', 'Luisa');
console.log(amigos);

/*
📌 .slice(): Método:
Nos permite copiar una parte de una arreglo a otro.
- 1er parametros - Posición desde donde queremos copiar.
- 2do parametros - Hasta antes de que elemento copiar. (parametro -1)
*/
const frutas = ['Fresa', 'Manzana', 'Uva', 'Piña', 'Mango', 'Naranja', 'Melon'];
console.log(frutas);
const misFrutasFavoritas = frutas.slice(1, 4);
console.log(misFrutasFavoritas);

/*
📌 .indexOf(): Método:
Obtenemos el primer index de un elemento, ojo con esto, solo el primero que encuentra, en un arreglo.
Si no hay elemenetos nos retorna -1
*/
const nombres = [
  'Carlos',
  'Rafael',
  'Estefania',
  'Rodrigo',
  'Rafael',
  'Gema',
  'Diana',
  'Sara',
];

let index = nombres.indexOf('Rafael');
console.log(index);

index = nombres.indexOf('Gema');
console.log(index);

index = nombres.indexOf('Sergio');
console.log(index);

/*
📌 .lastIndexOf(): Método:
Obtenemos el ultimo index de un elemento en un arreglo.
*/
let lastIndex = nombres.lastIndexOf('Rafael');
console.log(lastIndex);

/*
📌 .forEach(): Método: Recorrer.
Nos permite ejecutar una función por cada elemento del arreglo.
*/
nombres.forEach((nombre, index) => {
  console.log(`El usuario es: ${nombre} su indice: ${index}`);
});

/*
📌 .find(): Método: Encontrar.
Nos permite recorrer un arreglo y devuelve el 'PRIMER' elemento que retornemos.
*/
const resultado = nombres.find((nombre) => {
  let nombreE;

  if (nombre[0] === 'G') {
    nombreE = nombre;
  }
  return nombreE;
});

console.log(resultado);

/*
📌 .map(): Método:
Nos permite ejecutar una función por cada elemento y crear un nuevo areglo en base a los resultados de esa función.
*/
const nombresMay = nombres.map((nombre) => {
  let nombreMay = nombre.toUpperCase();
  return nombreMay;
});

console.log(nombresMay);

/*
📌 .filter(): Método:
Nos permite ejecutar una función por cada elemento y luego crear un arreglo en base a los resultados de esa función.
*/
const nombres4Letras = nombres.filter((nombre) => {
  let nombre4letras;
  if (nombre.length === 4) {
    nombre4letras = nombre;
  }
  return nombre4letras;
});

console.log(nombres4Letras);

/*
📌 .includes(): Método:
Nos permite saber si el arreglo contiene un elemento específico. Retorna true o false.
*/
let nombreIncludes = nombres.includes('Julio');
console.log(nombreIncludes);
nombreIncludes = nombres.includes('Rafael');
console.log(nombreIncludes);

/*
📌 .every(): Método:
Nos permite ejecutar una funcion y dentro un condicional sobre cada elemento y nos devuleve true si TODOS los elementos cumplieron la condición. O false si no cumple.
*/
const cadenaFrutas = ['Fresa', 'Manzana', 'Uva', 'Piña', 'Mango', 45];
console.log(cadenaFrutas);
const isAllString = cadenaFrutas.every((fruta) => {
  let result;
  if (typeof fruta === 'string') {
    result = true;
  } else {
    result = false;
  }
  return result;
});

console.log(`¿Todos los nombres son válidos?:${isAllString}`);

/*
📌 .some(): Método:
Nos permite ejecutar una funcion y dentro un condicional sobre cada elemento y nos devuelve true si algun elemento cumplio la condición.
*/

const isSomeNumber = cadenaFrutas.some((fruta) => {
  let isInValid;
  if (typeof fruta !== 'string') {
    isInValid = true;
  } else {
    isInValid = false;
  }
  return isInValid;
});

console.log(`Es arreglo es ínvalido: ${isSomeNumber}`);
