/*
Vamos a ver los métodos para objetos. {}
*/

const usuario = {
  nombre: 'Jose',
  edad: 34,
  amigos: ['Alejandro', 'Cesar', 'Manuel'],
  saludo: function () {
    console.log('Hola');
  },
};

/*
📌 Métodos propios
Los objetos pueden tener métodos personalizados.
*/
usuario.saludo();

/*
📌 Object.keys(): Método:
Nos devuelve un arreglo [] con las llaves (keys), las propiedades del objeto.
*/
const keys = Object.keys(usuario);
console.log(keys);

/*
📌 Object.values(): Método:
Nos devuelve un arreglo [] con las valores del objeto.
*/
const values = Object.values(usuario);
console.log(values);

/*
📌 Object.entries(): Método:
Nos devuelve un arreglo [] con las parejas de [[clave:valor]] del objeto.
*/
const pair = Object.entries(usuario);
console.log(pair);
