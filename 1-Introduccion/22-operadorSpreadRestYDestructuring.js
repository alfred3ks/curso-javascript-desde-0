/*
Operador Spread, Parametros Rest y destructuring de objetos, arreglos y en funciones.
*/

/*
📌 El operador Spread: ..., este operador es nuevo.
Permite tomar los elementos de un arreglo u objeto y expandirlo en otro sitio.
*/
const frutas = ['Manzana', 'Pera', 'Piña', 'Melon', 'Uvas'];
const comida = ['Pizza', 'Sushi', 'Pasticho'];
const favoritas = [...frutas, ...comida];
console.log(favoritas);

// Vemos con objetos:
const datosLogin = {
  correo: 'correo@correo.com',
  password: 123,
};

const usuario = {
  nombre: 'Maria',
  edad: 23,
  ...datosLogin,
};
console.log(usuario);

/*
📌 Paramatros rest: ..., este operador es nuevo.
Permite que una función contenga un número indefinido de argumentos. Los argumentos extras que encuentra los convertira en arreglos.
*/
const registrarUsuario = (nombre, correo, ...dataExtra) => {
  console.log(nombre, correo, dataExtra);
};

registrarUsuario('Margarita', 'margarita@correo.com');
registrarUsuario('Margarita', 'margarita@correo.com', 28, 'España');

/*
📌 Destructuración de objetos/arreglos.
Nos permite obtener elementos o propiedades de un arreglo u objeto y guardarlos en una variable.
*/
const amigos = ['Alejandro', 'Cesar', 'Manuel'];
const [primerAmigo, segundoAmigo, tercerAmigo] = amigos;
console.log(primerAmigo);
console.log(segundoAmigo);
console.log(tercerAmigo);

// Vemos desestructuring con objetos
const user = {
  nombre: 'John',
  apellido: 'Doe',
  pais: 'EEUU',
};

// NOTA: Aqui los valores deben ser los mismos del objeto:
const { nombre, pais } = user;
console.log(nombre);
console.log(pais);

// Vemos destructuración pasada a una funcion:
const mostrarEdad = ({ nombre, pais }) => {
  console.log(`${nombre} es de ${pais}.`);
};

// Pasamos todo el objeto completo:
mostrarEdad(user);
