/*
Promesas.
Las promesas son estructuras que vamos a definir para decirle al navegador a que espere a que nosotros términemos de hacer una operación.
*/

// Asi creamos una promesa:
const promesa = new Promise((resolve, reject) => {
  // Acción que se ejecutara.
  setTimeout(() => {
    const exito = false;

    if (exito) {
      resolve('Operación existosa.');
    } else {
      reject('La operación no fue exitosa.');
    }
  }, 4000);
});

promesa.then((mensaje) => {
  alert(mensaje);
});

// Asi capturamos los rechazos de la promesa.
promesa.catch((mensaje) => {
  alert(mensaje);
});
