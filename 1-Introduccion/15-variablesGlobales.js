/*
Las variables globales.
El scope, alcance o ambito de las variables.

Significa que desde que parte de nuestro código vamos a poder acceder a las variables.

📌 Global Scope o Variables Globales:
- Son las variables declaradas fuera de la función,
- Podemos acceder a ellas desde cualquier parte del código,
- Podemos usar var, const, let.
*/

// Variable declarada en el scope global:
var nombre = 'Marcos';
console.log(nombre);

const saludo = () => {
  // Vemos como dentro de una funcion podemos acceder a esa variable global.
  console.log(`Hola ${nombre}`);

  // Tambien podemos cambiar su valor desde cualquier sitio.
  nombre = 'Arturo';
  console.log(`El nuevo nombre es ${nombre}`);
};

saludo();
