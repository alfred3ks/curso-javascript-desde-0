/*
Ejemplo de promesas.
*/

const fetchPosts = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const posts = ['Post 1', 'Post 2', 'Post 3'];
      const error = false;

      if (error) {
        reject('Hubo un error al intentar de obtener los posts.');
      } else {
        resolve(posts);
      }
    }, 2000);
  });
};

console.log('Inicia operación');
fetchPosts()
  .then((posts) => {
    console.log(posts);
  })
  .catch((err) => {
    console.log(err);
  });
console.log('Finaliza operación.');
