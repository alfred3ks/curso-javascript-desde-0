/*
Los template string nos permitiran trabajar con variables dentro de cadenas de texto.

Para eso necesitamos las comillas invertidas. Las backticks. ``
*/

const nombre = 'Luis Marcano';
const edad = 27;
const pais = 'Italia';

// Ejemplo sin backticks:
console.log(
  'Hola soy ' + nombre + ', tengo ' + edad + ' años y vivo en ' + pais + '.'
);

// Ejemplo con backticks:
console.log(`Hola soy ${nombre}, tengo ${edad} años y vivo en ${pais}.`);
