/*
Los objetos en JavaScript. {}
En JavaScript, un objeto es un entidad independiente con propiedades y tipos. Compárarlo con una taza, por ejemplo. Una taza es un objeto con propiedades. Una taza tiene un color, un diseño, un peso, un material del que está hecha, etc. Del mismo modo, los objetos de JavaScript pueden tener propiedades que definan sus características.

Vemos como son valores clave-valor.

Las variables dentro d eun objeto son propiedades y las funciones son métodos.

*/

const persona = {
  nombre: 'Luis',
  edad: 27,
  correo: 'correo@correo.com',
  isDev: true,
  isActive: true,
  suscripciones: {
    Web: true,
    Correo: false,
  },
  coloresFavoritos: ['rojo,', 'verde', 'amarillo'],
  saludo: function () {
    console.log('Hola, soy ' + this.nombre);
  },
};

// Asi accedemos a los valores del objeto:
console.log(persona);
console.log(persona.nombre);
console.log(persona['nombre']);

const data = 'correo';
console.log(persona[data]);

const suscripciones = 'suscripciones';
console.log(persona[suscripciones]);
console.log(persona[suscripciones]['Web']);
console.log(persona[suscripciones]['Correo']);

console.log(persona.coloresFavoritos);

// Podemos añadir mas propiedades al objeto:
persona.pais = 'España';
persona.descripcion = 'El mejor país del mundo';

console.log(persona);
console.log(persona.pais);
console.log(persona.descripcion);

// Asi accedemos a los métodos: accedemos a ellos invocandolos.
persona.saludo();
