/*
Ciclo while.
Nos permite hacer bucles tambien pero con una estructura muy diferente. Este no se usa mucho pero debemos saber que existe. Miestras se cumpla una condicion se ejecutara un bloque de código.
Con while tenemos que tener cuidado con los ciclos infinitos. Necesitamos un contador o una condición que parara el ciclo. Esta variable suele estar fuera del ciclo.
*/

/*
📌 Ciclo while:
Es similar al for, pero con la diferencia de que solo tenemos un condicional. Mientras se cumpla el condicional se ejecutara el ciclo.
*/
let contador = 0;
while (contador <= 10) {
  console.log(contador);
  contador++;
}

/*
📌 Ciclo do while:
Es similar al while, con la diferencia de que ejecuta el bloque de código al menos una vez.
*/
let i = 11;
do {
  console.log(i);
  i++;
} while (i <= 10);
