/*
Los Arreglos. dentro de JavaScript son objetos, y los objetos tienen propiedades y metodos.
*/

const arreglo = ['texto', 245.1, false, { nombre: 'Luis' }, [1, 2, 3]];
console.log(arreglo);

// Veamos como acceder a los valores de un arreglo:
const amigos = ['Alejandro', 'Manuel', 'Cesar'];
console.log(amigos);
// Accedemos a los valores del arreglo
console.log(amigos[0]);
console.log(amigos[1]);
console.log(amigos[2]);

// Otro ejemplo: Como agregar valores a un arreglo: vemos como podemos saltarnos elementos, aunque eso no es muy comun hacerlo.
const colores = [];
colores[0] = 'Rojo';
colores[1] = 'Verde';
colores[3] = 'Blanco';
// Sobreescribimos el valor:
colores[3] = 'Amarillo';
console.log(colores);

// Vemos como usar los métodos y propiedades:
console.log(`El arreglo colores tiene ${colores.length} colores.`);
colores.push('Azul');
console.log(colores);
