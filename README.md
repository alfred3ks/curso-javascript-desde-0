# Curso de JavaScript desde cero - 2023.

## Tutor: Carlos Arturo Esparza - FalconMasters
## Curso en la plataforma de Udemy:
[Curso de JavaScript desde cero](https://www.udemy.com/course/curso-javascript-desde-cero/)

Temario del curso:

- Introducción a JavaScript.
- Módulos (ES6).
- Browser Object Model.
- Document Object Model - DOM.
- Proyecto - Galeria de imágenes.
- Proyecto - Página de producto de una tienda.
- Formularios.
- Proyecto - Formulario por pasos.
- APIs.
- Proyecto - App de películas.
- Proyecto - App de gastos.
- Proyecto - Portfolio de trabajos.

A continuación tenemos los link a los repos de los proyectos.
1. [Galería de imágenes](https://github.com/alfred3ks/galeria-imagenes-javascript)

## Alfredo Sánchez - @alfred3ks