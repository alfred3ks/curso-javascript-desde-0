/*
En el BOM, browser object model.
Ahora vamos a ver el objeto Location y el objeto History.
Vamos a ver como podemos acceder a la barra de direcciones del navegador. Podemos ver la direccion actual y ademas cargar nuevos documentos.
*/

// 📌 Objeto location:
console.log(location); // Nos retorna un objeto.
console.log(location.href); // Vemos la url
console.log(location.host); // Vemos el host de la web
console.log(location.hostname); // Vemos el hostname de la web
console.log(location.port); // Vemos el puerto
console.log(location.pathname); // Vemos la ruta del archivo
console.log(location.protocol); // Vemos el protocolo http o https

const cargarDocumento = () => {
  // El método assign() recibe la url de la web que queremos cargar
  location.assign(
    'https://developer.mozilla.org/en-US/docs/Web/API/Location/reload'
  );
};

/*
📌 Objeto history: Nos permite acceder a los botones del navegador para ir para adelante o atras en las paginas.
Tenemos dos metodos forward() y back()

Este objeto no se utiliza mucho.
*/

const irAdelante = () => {
  history.forward();
};
