/*

Funciones de tiempo: Tenemos 2 métodos para poder trabajar con tiempo en el navegador.

Por ejemplo si queremos ejecutar nuestro codigo pasado un tiempo. O cada x tiempo.

*/

/*
📌 setTimeOut():
Nos permite ejecutar una función despues de cierto tiempo.
Tambien vemos como parar ese timer con otro método.
*/
let id;
const iniciar = () => {
  console.log('Iniciando timer');
  id = setTimeout(() => {
    alert('Hola, desde un setTimeout');
  }, 5000);
};

const parar = () => {
  console.log('Parando timer');
  clearTimeout(id);
};

/*
📌 setInterval()
Nos permite ejecutar una funcion cada cierto tiempo.
*/
let idInterval;
const iniciarContador = () => {
  console.log('Iniciamos contador.');
  let contador = 0;
  idInterval = setInterval(() => {
    console.log(contador);
    contador++;
  }, 1000);
};

const pararContador = () => {
  console.log('Paramos contador.');
  clearInterval(idInterval);
};
