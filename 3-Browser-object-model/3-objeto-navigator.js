/*

Objeto navigator.

Con este objeto podemos saber si las cookies estan activadas, información del lenguaje del navegador. Esto se conoce como user agent.

*/

console.log(`Coockies activadas: ${navigator.cookieEnabled}`);
console.log(`UserAgent: ${navigator.userAgent}`);
console.log(`Lenguaje del navegador: ${navigator.language}`);
console.log(`El navegador esta conectado a internet: ${navigator.onLine}`);
