/*
Browser Object Model: BOM

Esto es un objeto que nos va a permitir a acceder a propiedades y métodos que nos da el navegador web.
Con este objeto podemos acceder por ejemplo a la ventana del navegador, conocer su tamaño, acceder a la barra de direcciones, podemos crear nuevas pestañas, podemos trabajar con las alertas del navegador.

Por medio del objeto llamado window podemos acceder a esas propiedades y métodos.

*/

console.log(window);

/*
Podemos acceder a los metodos y propiedades del objeto window sin escribir el objeto, ya que esos métodos y propiedades son globales, con llamarlos es suficiente.

window.alert('Hola)
alert('Hola)
*/

// Vemos como acceder a los metodos y propiedades:
console.log(`La ventana del navegador mide ${innerWidth}px de ancho`);
console.log(`La ventana del navegador mide ${innerHeight}px de alto`);

/*
📌 .open(): No es muy comun abrir ventanas con JavaScript.
Nos permite abrir ventanas del navegador.
Nota: Es posible que el navegador te pìda permisos para abrir una nueva ventana.
Este método recibe tres parametros:
- 1er parametro: Dirección de la nueva ventana, opcional, puede ser en blanco.
- 2do parametro: Nombre de la ventana, opcional
- 3er parametro: Cadena de texto de opciones. opcional. Por ejemplo abrir la ventana en un tamaño especifico.

Nos devuelve un objeto para acceder a esa ventana.

Por medio del boton en el html vamos a abrir una ventana usando el metodo.
*/

let ventana;
const abrirVentana = () => {
  ventana = open('', '', 'width=700, height=700');
  ventana.document.write('<h1>Hola Mundo</h1>');
};

const cerrarVentana = () => {
  ventana.close();
};

/*
📌 Screen Object: screen:
Este objeto representa la pantalla del usuario. El ancho y alto del monitor, tiene en cuenta las barras de windows.
*/

console.log(`Ancho de pantalla ${screen.width}`);
console.log(`Alto de pantalla ${screen.height}`);

console.log(
  `Ancho de pantalla sin barra de window ${window.screen.availWidth}`
);
console.log(
  `Alto de pantalla sin barra de window ${window.screen.availHeight}`
);

/*
No se porque pero a mi todos me salen de la misma medida.
*/
