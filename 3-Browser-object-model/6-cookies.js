/*
Las cookies:

Este tema es muy importante dentro del mundo web, son archivos de texto que guardan información en el pc del usuario. Estos archivos de texto se usan para guardar información que se guardan y al refrescar la página esta no se pierde.

Las cookies son cadenas de texto.

Ahora esa cookie se guarda en la pestaña de Aplicación/Cookies:

*/

// Creamos una cookie:
const crearCookie = () => {
  // Formato de cookie solo de sesion:
  // document.cookie = 'nombre=Luis';
  // Formato con fecha de expedicion:
  document.cookie = 'nombre=Maria; expires=26 August 2023 01:00:00 UTC';
};

// Capturamos el nombre de usuario por medio del prompt:
const iniciarSesion = () => {
  const user = prompt('Usuario:');
  document.cookie = `nombre=${user}; expires=26 August 2023 01:00:00 UTC`;
  alert('Sesión iniciada...');
};

// Para leer las cookies:
console.log(document.cookie);

let usuarioCookie;
const cookies = document.cookie.split(';');
console.log(cookies);

cookies.forEach((cookie) => {
  // filtramos la cookie: Como extraer la información:
  const propiedad = cookie.split('=')[0];
  if (propiedad === 'nombre') {
    console.log(cookie);
    usuarioCookie = cookie.split('=')[1];
  }
});

console.log(usuarioCookie);

if (usuarioCookie) {
  console.log('Hola ' + usuarioCookie);
} else {
  console.log('Por favor inicia sesión.');
}

// Como borrar cookie:
const cerrarSesion = () => {
  // Ponemos el valor en vacio y con una fecha pasada.
  document.cookie = 'nombre=; expires=22 August 2023 01:00:00 UTC';
  console.log('Hasta luego...');
};
