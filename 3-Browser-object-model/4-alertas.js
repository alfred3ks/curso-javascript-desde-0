/*

Alertas:

Vamos a ver como podemos acceder a las ventanas de alertas del navegador. Dentro del navegador vamos a tener tres tipos de ventanas diferentes.

*/

/*
📌 alert():
Para informar al usuario de algo.
*/
const alertSaludo = () => {
  alert('Hola usuario');
};

/*
📌 confirm(): Ventana de confirmación:
Una ventana donde el usuario puede aceptar o cancelar. Retorna true o false.
*/

const ingresar = () => {
  const resp = confirm('¿Eres mayor de edad?');
  if (resp) alert('Bienvenido...');
  else alert('No puedes ingresar...');
};

/*
📌 prompt(): Ventana con input:
Una ventana donde el usuario puede introducir un valor, retorna una cadena de texto con la respuesta del usuario.
*/

const saludar = () => {
  const nombre = prompt('Ingresa tu nombre');
  alert(`El nombre ingresado es ${nombre}`);
};
