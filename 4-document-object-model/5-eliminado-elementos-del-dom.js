/*
Eliminado elementos del DOM:

Ahora vamos a ver como eliminar elementos del DOM. Creamos un boton para eliminar.

Para eliminar un elemento tenemos el método:

.removeChild() -> Para eliminar un elemento hijo.

*/

const eliminarCaja = () => {
  const contenedorPadre = document.getElementById('contenedor1');
  const cajaAEliminar = contenedorPadre.querySelector('.caja');
  if (cajaAEliminar) {
    contenedorPadre.removeChild(cajaAEliminar);
  }
};
