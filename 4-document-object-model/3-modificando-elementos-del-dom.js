/*
Modificando elementos del DOM:
Para modificar un elemento del DOM lo haremos por medio de sus propiedades y métodos.
*/

/*
📌 elemento.innerHTML:
Nos permite cambiar el contenido de HTML de un elemento:
*/

const primeraCaja = document.querySelector('.caja');
console.log(primeraCaja);
console.log(primeraCaja.innerHTML);

primeraCaja.innerHTML = '<b>Box 1</b>';
console.log(primeraCaja.innerHTML);

/*
📌 elemento.attribute:
Nos permite acceder y cambiar atributos del elemento: Tambien existe otra forma por medio del método setAttibute()
📌 elemento.setAttibute(): Recibe dos parametros.
Nos permite agregar o establecer el valor de un atributo del elemento.
Si ya tenemos un atributo en el elemento este la sobre escribe.
Tambien podemos agregar atributos personalizado, estos se suelen agregar data-algo.
*/

primeraCaja.id = 'box-1';
console.log(primeraCaja);

primeraCaja.setAttribute('class', 'caja box-1');
console.log(primeraCaja);

primeraCaja.setAttribute('data-id', '123-456-789');
console.log(primeraCaja);

/*
📌 elemento.style.property:
Nos permite cambiar los estilos css de un elemento.
Las propiedades de mas de una palabra debemos usar camelCase.
Al usar poner estilos a un elemento este se agrega en linea.
*/

const contenedor2Caja1 = document.querySelector('#contenedor2 .caja');
console.log(contenedor2Caja1);
contenedor2Caja1.style.backgroundColor = '#000000';
contenedor2Caja1.style.color = '#ffffff';
contenedor2Caja1.style.textTransform = 'uppercase';
