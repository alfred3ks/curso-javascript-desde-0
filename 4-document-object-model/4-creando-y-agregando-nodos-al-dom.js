/*
Creando y agregando elementos al DOM:
Vamos a crear un boton que al hacer click vamos a agregar una caja.
Tenemos varios metodos:
- createElement()
*/

const agregarCaja = () => {
  /*
  📌 1. Creamos el elemento:
  createElement - Recibe como parametro una cadena de texto con la etiqueta que queremos crear.
  */

  const nuevaCaja = document.createElement('div');

  /*
  📌 2. Agregamos el texto y atributos que tendra ese elemento.
  */

  nuevaCaja.innerText = 'New Box';
  nuevaCaja.setAttribute('class', 'caja activa');

  /*
  📌 3. Ahora agregamos el elemento al DOM:
  Tenemos varios métodos de hacerlo:
  1- appenChild()- Agrega un elemento al final.
  2- insertAdjacentElement():
  Nos permite agregar un elemento:
  Valores:
  afterbegin: como primer elemento:
  beforebegin: antes del elemento padre.
  beforeend: como ultimo elemento.
  afterend: despues del elemento padre.

  3- replaceWith(): Nos permite reemplazar un elemento por otro.

  Ademas tenemos que decirle donde la vamos a agregar. Debemos obtener a donde le vamos a agregar.
  */

  const contenedor1 = document.getElementById('contenedor1');
  // contenedor1.appendChild(nuevaCaja);

  // contenedor1.insertAdjacentElement('afterbegin', nuevaCaja);
  // contenedor1.insertAdjacentElement('beforebegin', nuevaCaja);
  // contenedor1.insertAdjacentElement('beforeend', nuevaCaja);
  // contenedor1.insertAdjacentElement('afterend', nuevaCaja);

  const contenedor1Caja1 = document.querySelector('#contenedor1 .caja');
  console.log(contenedor1Caja1);
  contenedor1Caja1.replaceWith(nuevaCaja);
};
