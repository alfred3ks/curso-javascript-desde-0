/*
Tipos de propagación:
Existen dos tipos de propagación de eventos.
- Propagación de tipo bubbling (false, por defecto): Pimero se ejecuta el evento del hijo y luego del padre.
En bubblig el evento del elemento hijo reacciona primero y despues el evento del elemento padre.
- Propagación de tipo capturing (true): Primero se ejecuta el evento del padre y luego el del hijo.
En capturing el elemento padre reacciona primero y despues el del hijo.

Nuestra funcion addEventListener() recibiria un tercer parametro, true o false, si no pasamos nada seria el que por defecto es. si es un hijo sera bubbling.
Con el valor de true o false lo que hacemos es invertir cual se ejecuta primero. Este valor booleano solo se coloca en el padre. En las hijos no se pone.
*/

const contenedor1 = document.getElementById('contenedor1');
contenedor1.addEventListener(
  'click',
  (e) => {
    console.log('Click en el contenedor1');
  },
  true
);

// Este evento se propaga por event bubbling:
const primeraCaja = document.querySelector('#contenedor1 .caja');
primeraCaja.addEventListener('click', (e) => {
  console.log('Click sobre la caja 1');
});
