/*
Modificando estilos de elementos del DOM:

Para modifciar los estilos tenemos dos formas de hacerlo:
- 📌 Modificar estilos mediante clases:
Para saber que clases tiene ese elemento tenemos una propiedad llamada .classList.
Con esta propiedad podemos añadir y eliminar clases tambien.
Para añadir una clase tenemos el método add().
Para eliminar tenemos el método remove()

Tambien tenemos un metodo que podemos usar para alternar clases:
Ese método es toggle(), toogle= alternar.

Tambien podemos comprobar si un elemento tiene o no una clase. Para eso tenemos el método contains(), nos permite comprobar si un elemento tiene o no esa clase.

- 📌 Moficando el atributo style con JavaScript:
Esto se conoce como los estilos en linea, inline styles.
Esto es si no queremos trabajar sin clases.


*/

// - 📌 Modificar estilos mediante clases:
const primeraCaja = document.querySelector('#contenedor1 .caja');
console.log(primeraCaja);
console.log(primeraCaja.classList);

const añadirClase = () => {
  primeraCaja.classList.add('activa');
  console.log(primeraCaja.classList);
};

const eliminarClase = () => {
  primeraCaja.classList.remove('activa');
  console.log(primeraCaja.classList);
};

const toogleClase = () => {
  primeraCaja.classList.toggle('activa');
  console.log(primeraCaja.classList);
};

const comprobarClase = () => {
  if (primeraCaja.classList.contains('activa')) {
    console.log(`La caja tiene la clase activa.`);
  } else {
    console.log(`La caja no tiene la clase activa.`);
  }

  console.log(`Y contiene las siguientes clases:`);
  primeraCaja.classList.forEach((clase) => {
    console.log(clase);
  });
};

// - 📌 Moficando el atributo style con JavaScript:
const ultimaCaja = document.querySelector('#contenedor2 .caja:last-child');
console.log(ultimaCaja);

ultimaCaja.style.background = '#000';
ultimaCaja.style.color = '#fff';

const cajas = document.querySelectorAll('.caja');
console.log(cajas);

let tamanioFuente = 24;
const incrementarFuente = () => {
  cajas.forEach((caja) => {
    caja.style.fontSize = `${tamanioFuente}px`;
    tamanioFuente++;
  });
};

const disminuirFuente = () => {
  cajas.forEach((caja) => {
    caja.style.fontSize = `${tamanioFuente}px`;
    tamanioFuente--;
  });
};
