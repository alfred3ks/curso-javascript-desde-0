/*
Seleccionar elementos del DOM:

Vamos a ver como podemos acceder a los diferentes elementos que tenemos en nuestra página web. Al cargar nuestra página se genera un objeto, el DOM. Este DOM contiene la estructura de nuestra página, sus estilos y mas cosas.

Vamos a ver como acceder a cada uno de esos elementos del DOM.

El DOM, este objeto nos entrega atributos y métodos para poder acceder a sus elementos.
*/

/*
📌 getElementById():
Nos permite obtener un elemento en base a su atributo id.
Al obtener ese elemento se convierte en un objeto, por eso podemos usar propiedades y métodos.
*/

const contenedor1 = document.getElementById('contenedor1');
console.log(contenedor1);
console.log(typeof contenedor1); // object

/*
📌 Propiedad: .children:
Nos permite obtener los elementos hijos.
Normalmente nos devuelve una colección de elementos HTML, un HTMLCollection. Como vemos no es un array, si queremos hacer cosas que hacemos con los array lo debemos convertir a array.
Las propiedades de HTMLCollection tiene muy poquitos atributos y métodos.
*/

console.log(contenedor1.children);
console.log(contenedor1.children.length);

/*
📌 Propiedad: .parentElement:
Nos permite obtener el elemento padre de un elemento.
*/

console.log(contenedor1.parentElement);

/*
📌 getElementByTagName():
Nos permite obtener un elemento en base a su etiqueta tag.
NOTA. Nos retorna una colección de HTML. Una colección no es un arreglo.
*/

const divs = document.getElementsByTagName('div');
console.log(divs); // HTMLCollection
console.log(`Tenemos ${divs.length} div en la página.`);

/*
📌 getElementsByClassName():
Nos permite obtener una coleccion de elementos en bse a su clase CSS.
NOTA. Nos retorna una colección de HTML. Una colección no es un arreglo.
*/

const contenedores = document.getElementsByClassName('contenedor');
console.log(contenedores);
console.log(
  `Tenemos ${contenedores.length} elementos con esa clase en la página.`
);

/*
📌 querySelector():
Nos devuelve el primer elemento que coincida con un selector estilos CSS.
NOTA: Nos retorna una NodeList.
Esta forma de seleccionar elementos es mucho mas versatil para buscar elementos.
*/

const caja1 = document.querySelector('#contenedor1 .caja');
console.log(caja1); // Nos devuelve el primer elemento que encuentra.

/*
📌 querySelectorAll():
Nos devuelve una coleccion de elemento que coincidan con un selector estilos CSS.
NOTA: Nos retorna una NodeList.
Este NodeList si tiene el metodo forEach()
*/

const cajas = document.querySelectorAll('#contenedor1 .caja');
console.log(cajas);

cajas.forEach((caja) => {
  console.log(caja);
});

/*
📌 closest:
Nos permite hacer una busqueda de adentro hacia afuera en busca de un elemento.
Partimos de un hijo a un ancestro.
*/

const caja3 = document.querySelector('.caja:last-child');
console.log(caja3);

const contenedorPrincipal = caja3.closest('.contenedor-principal');
console.log(contenedorPrincipal);

/*
📌 Podemos encadenar todos los métodos vistos:
*/

const contenedor2 = document.getElementById('contenedor2');
console.log(contenedor2);
const caja = contenedor2.querySelector('.caja');
console.log(caja);
