/*
Como eliminar eventos del DOM:
Asi como tenemos un método para agregar evento, addEventListener() tenemos uno para eliminarlos, removeEventListener()
*/

const btn1 = document.getElementById('btn1');
const btn2 = document.getElementById('btn2');
const caja1 = document.querySelector('#contenedor1 .caja');

const toogleClase = () => {
  caja1.classList.toggle('activa');
};

btn1.addEventListener('click', () => {
  console.log('Evento creado');
  caja1.addEventListener('click', toogleClase);
});

btn2.addEventListener('click', () => {
  console.log('Evento eliminado.');
  caja1.removeEventListener('click', toogleClase);
});
