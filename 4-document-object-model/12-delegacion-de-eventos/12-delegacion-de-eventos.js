/*
Delegación de eventos:

Vamos a ver como delegar eventos, asi no agregamos muchos eventos en nuestros elementos y que impacten en el performance de la aplicación.

Vamos a ver como es la forma menos optimizada y cual es la forma que debemos hacer bien. Lo vemos en los dos archivos anexos.

Aqui vemos como desde el padre podemos delegar eventos y aprovecharnos de esta particularidad que nos aporta el navegador con el DOM.

*/
