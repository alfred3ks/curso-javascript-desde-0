// Forma menos optimizada:
const lista = document.getElementById('lista');
const btnAgregar = document.getElementById('btn-agregar');

// Agregamos el icono:
const agregarListener = () => {
  lista.childNodes.forEach((elemento) => {
    elemento.addEventListener('click', (e) => {
      e.target.classList.toggle('activo');
    });
  });
};

agregarListener();

// Creamos elementos:
btnAgregar.addEventListener('click', () => {
  const elemento = `
		<a href="#">
			Elemento <i class="bi bi-check-square-fill"></i>
		</a>
	`;

  lista.innerHTML += elemento;
  agregarListener();
});
