/*
Como agregar eventos:
Tenemos dos formas de agregar eventos a los elementos del DOM:
- Directamente sobre el elemento ponerle como atributo el evento, como lo hacemos con los botones vistos anteriormente:

<button onclick="eliminarClase()">Eliminar clase</button>

Esta forma no la debemos usar, no es la recomendada.

- La segunda forma usando el metodo addEventListener().
Este metodo nos permite adjuntar un evento al elemento.
recibe dos parametros, uno el evento y de segundo una funcion que se ejecutara al dispararse el evento.

*/

const btnToogle = document.getElementById('btn-toogle');
console.log(btnToogle);

const cajas = document.querySelectorAll('.caja');
console.log(cajas);

btnToogle.addEventListener('click', (e) => {
  console.log(e);
  console.log(e.target);
  cajas.forEach((caja) => {
    caja.classList.toggle('activa');
  });

  if (!btnToogle.classList.contains('back-color')) {
    btnToogle.classList.add('back-color');
  } else {
    btnToogle.classList.remove('back-color');
  }
});

/*
Vamos a añadir eventos que cuando se haga click en una caja lo muestre por consola:
📌 Agreando el evento a multiples elementos:
Podemos agregar el evento a multiples elementos recorriendo medieante ciclos.
NOTA: Esta no es la forma optima. Ya veremos como hacerlo bien.
No esta bien porque estamos registrando un evento por elemento. Imaginate que tenemos 1000 cajas. Eso no ayuda al performance del navegador.
 */

cajas.forEach((caja) => {
  caja.addEventListener('click', (e) => {
    caja.classList.toggle('activa');
    console.log(e.target.innerText);
    console.log(`Has realizado click sobre: ${e.target.innerText} `);
  });
});
