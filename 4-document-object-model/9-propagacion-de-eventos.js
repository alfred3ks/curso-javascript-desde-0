/*
Propagación de eventos:
La propagación de eventos es un comportamiento que tiene por defecto el navegador.
En el ejemplo creamos un evneto en el contenedor padre y vemos como se propaga el evento hacia los hijos, hacemos click en los hijos y s ejecuta el evento.

Tambien vemos que existe un efecto que se llama event bubbling:
Evento Bubbling es un concepto en el DOM (Modelo de Objeto del Documento). Sucede cuando un elemento recibe un evento, y ese evento burbujea (o puedes decir que es transmitido o propagado) a sus elementos, padres y ancestros en el árbol del DOM hasta que llega al elemento raíz.

Para evitar esto tenemos un metodo e.stopPropagation(), esto se hace sobre un elemento hijo.
*/

const contenedor1 = document.getElementById('contenedor1');
contenedor1.addEventListener('click', (e) => {
  console.log('Click en el contenedor1');
});

// Este evento se propaga por event bubbling:
const primeraCaja = document.querySelector('#contenedor1 .caja');
primeraCaja.addEventListener('click', (e) => {
  // Propagación sobre el elemento hijo:
  e.stopPropagation();
  console.log('Click sobre la caja 1');
});
