/*
📌´Defaults import: Al importar ya no hacen falta las llaves. {}
*/

// 📌 Primera opcion de importar, tambien podemos poner el nombre que queramos:
import obtenerUsuario from './defaultExports';
console.log(obtenerUsuario());

//📌 Segunda opcion de importar: Podriamos llamar la variable como queramos:
import obtener from './defaultExports';
console.log(obtener());
