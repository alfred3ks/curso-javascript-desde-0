'use strict';

/*
📌 Vamos a ver todas las formas que tenemos para importar y exportar archivos default.
Es el caso en el que exportamos una sola cosa, esto es muy comun verlo en react.
*/

/*
📌 Primera opción:
*/
// export default () => {
//   return {
//     nombre: 'Cesar',
//     correo: 'cesar@correo.com',
//   };
// };

/*
📌 Segunda opción:
*/
const obtenerUsuario = () => {
  return {
    nombre: 'Cesar',
    correo: 'cesar@correo.com',
  };
};

/*
📌´Defaults import: Al importar ya no hacen falta las llaves. {}
*/

console.log(obtenerUsuario());
console.log(obtenerUsuario());
