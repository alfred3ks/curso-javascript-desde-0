'use strict';

/*
📌 Named export
*/
// Podemos exportar funciones y variables:
// 📌 Forma 1 - Palabra export:
// export const nombre = 'Alfredo';

// export const obtenerPosts = () => {
//   return ['Post1', 'Post2', 'Post3'];
// };

// 📌 Forma 2 - Export al final:
const nombre = 'Alfredo';

const obtenerPosts = () => {
  return ['Post1', 'Post2', 'Post3'];
};

/*
📌 Named Imports, primera forma de importar:
*/
// import { nombre, obtenerPosts } from './nameExport';

console.log(`Mi nombre es ${nombre}`);
console.log(obtenerPosts());
