/*
Vamos a ver todas las formas que tenemos para importar y exportar archivos.

📌📌 namedExport.js: Exportar código:
Forma 1 - Palabra export:
export const nombre = 'Alfredo';

export const obtenerPosts = () => {
  return ['Post1', 'Post2', 'Post3'];
};

Forma 2 - Export al final:
const nombre = 'Alfredo';

const obtenerPosts = () => {
  return ['Post1', 'Post2', 'Post3'];
};

export { nombre, obtenerPosts };

📌📌 index.js: Importar código:
📌 Named Imports, primera forma de importar:

import { nombre, obtenerPosts } from './nameExport';

📌 Tambien podemos importar con un alias:
import { nombre as nombreBis, obtenerPosts } from './nameExport';

console.log(`Mi nombre es ${nombre}`);
console.log(`Mi nombre es ${nombreBis}`);
console.log(obtenerPosts());

📌 Namespace Imports, segunda forma de importar: Esta opcion no es muy usada la primera opcion es la que mas se usa.

import * as datos from './nameExport';
console.log(`Mi nombre es ${datos.nombre}`);
console.log(datos.obtenerPosts());

*/
