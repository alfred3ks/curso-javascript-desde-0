/*
📌 Named Imports, primera forma de importar:
*/
// import { nombre, obtenerPosts } from './nameExport';

// 📌 Tambien podemos importar con un alias:
// import { nombre as nombreBis, obtenerPosts } from './nameExport';

// console.log(`Mi nombre es ${nombre}`);
// console.log(`Mi nombre es ${nombreBis}`);
// console.log(obtenerPosts());

/*
📌 Namespace Imports, segunda forma de importar: Esta opcion no es muy usada la primera opcion es la que mas se usa.
*/
import * as datos from './nameExport';
console.log(`Mi nombre es ${datos.nombre}`);
console.log(datos.obtenerPosts());
