/*
📌 Named export
*/
// Podemos exportar funciones y variables:
// 📌 Forma 1 - Palabra export:
// export const nombre = 'Alfredo';

// export const obtenerPosts = () => {
//   return ['Post1', 'Post2', 'Post3'];
// };

// 📌 Forma 2 - Export al final:
const nombre = 'Alfredo';

const obtenerPosts = () => {
  return ['Post1', 'Post2', 'Post3'];
};

export { nombre, obtenerPosts };
