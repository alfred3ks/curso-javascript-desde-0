/*
Instalando rollup:

Lo primero que debemos de hacer es instalar el package.json.

  npm init -y

¿Que es rollup?
Rollup es una herramienta Javascript orientada a unir todo el Javascript de nuestra aplicación (generalmente dividido en diferentes ficheros) en un único fichero .js (lo que se conoce como bundler), realizando por el camino ciertas transformaciones de código para mejorar algunos aspectos.

Ahora procederemos a installar rollup: Lo instalamos en nuestro proyecto. Lo instalamos como dependencia de desarrollo. Vemos el comando en la pagina de rollup en su documentacion.

https://rollupjs.org/
https://rollupjs.org/tutorial/#installing-rollup-locally

Tenemos dos comando que hacen lo mismo:

  npm install rollup --save-dev
  npm install -D rollup

Lo comprobamos en el archivo package.json que se instalo como devDependencies.

Ahora ya pasamos a usar rollup despues de estar instalado.

Para llamar y usar a rollup como este se ha instalado como dependecia de desarrollo debemos usar el siguiente comando:

  npx rollup

Debemos crear un archivo de configuracion para que rollup nos genere el bundle.js que sera el que leera nuestro index.html.

Ahora vamos a crear una estructura de carpetas necesarias en los proyectos:

src: aqui meteremos todos los archivos de .js que creemos.

public: Aqui meteremos todos los archivos que seran publicos para el navegador. La carpeta css, las imagenes, etc. aqui es donde se compilara el archivo final bundle.js.

Dentro de src creamos los archivos:

  index.js
  carrito.js

Ahora vamos a compilar con rollup: Debemos crear un archivo de configuración:

  rollup.config.js

export default {
  input: 'src/index.js',
  output: {
    file: 'public/bundle.js',
    format: 'cjs',
  },
};

Creamos dentro del package.json y nos creamos el comando que hara que rollup este analizando nuestro código y cuando se generen cambio compile. Se quedara observando cambios y si deseamos pararlos lo hacemos con ctrl + c.

  "build": "rollup --config --watch"

Ademas en nuestro package.json le tenemos que decir que nuestos archivos sonde tipo module.

{
  "name": "1-modulos",
  "version": "1.0.0",
  "type": "module",
  "description": "",
  "main": "instalandoRollup.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "build": "rollup --config --watch"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "rollup": "^3.26.3"
  }
}

  npm run build

Vemos como en la carpeta public se genera el archivo bundle.js.

*/
